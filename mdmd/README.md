# Material风格的Markdown编辑器
(开发中...)

使用Vue.js + Vuetify开发


## 主界面

![main](./screenshot/mdmd1.png)
左边是编辑区域,右边为显示区域。显示区的的滚动位置跟随编辑区变动。


## 设置界面

![setting](./screenshot/mdmd2.png)

提供文章的增/删/改/查/导出功能。数据存储在IndexedDB数据库中(开发中...)