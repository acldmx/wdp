import { openDB } from "idb"

const tablename = "article"

const db = openDB('mdmd', 1, {
    upgrade(db) {
        db.createObjectStore(tablename, {
            autoIncrement: true
        });
    }
})

export default function install(Vue, options) {
    Vue.prototype.$db = {

        /**
         * 
         * @param {String} title 
         * @param {String} content 
         */
        async createArticle(title, content) {
            return (await db).add(tablename, {
                title: title,
                content: content
            })
        },
        /**
         * 
         * @param {Number} id 
         */
        async deleteArticle(id) {
            return (await db).delete(tablename, id)
        },
        /**
         * 
         * @param {Number} id 
         * @param {String} title 
         * @param {String} content 
         */
        async updateArticle(id, title, content) {
            return (await db).put(tablename, {
                title: title,
                content: content,
            }, id)
        },
        async getAll() {
            return (await db).getAll(tablename)
        }
    }
}