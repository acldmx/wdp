# MURMUR
**单页白噪音应用**

Vue.js新手练习小项目

一个简单的前端项目

支持10种不同的白噪音,组合设置不同的白噪音组合成各种合适的播放计划

## 主界面预览
![Alt text](./extra/murmur.png)

## 实现功能

1. 播放定时/延迟播放
![Alt text](./extra/murmur2.png)

2. 计时播放状态显示
![Alt text](./extra/murmur3.png)

3. 播放计划管理
![Alt text](./extra/murmur4.png)

4. 设置各个白噪音的声音
![Alt text](./extra/murmur5.png)